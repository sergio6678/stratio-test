# stratio-test


# Description

Design and implement the backend for a presence control system for the employees of a
company. You need to take into consideration that the employees must always clock in and
out by registering their fingerprint.
The “People” department will need:

• Access to the employee’s presence and time control.

• Generation of time sheets of effective worked hours.

• Reception of asynchronous notifications regarding: total absence time, absence anomalies
(for example because of on-call duties), etc.


# Architecture Diagram


![Scheme](documents/presence_control.jpg)

# Installation 


1.Install pip 

2.Install flask  
```
sudo apt install python3-flask
```

```
sudo apt-get install python3-pymysql
```


3.Install mysql  
```
sudo apt update
sudo apt install mysql-server
sudo mysql_secure_installation
```
4.Create table with employees access :

```mysql -uroot -p < sql/ sql/database_employees.sql ```


RUN FLASK SITE

```FLASK_APP=main.py flask run```




http://127.0.0.1:5000/employees (list all Employees)

http://127.0.0.1:5000/employees_presence_on/ (list all employees with actual clock in status)

http://127.0.0.1:5000/employees_abscence (AJAX PULL over absence_anomalies table)




5.Install Confluent Plataform

https://docs.confluent.io/current/installation/installing_cp/zip-tar.html


```
./bin/zookeeper-server-start etc/kafka/zookeeper.properties > /dev/null 2>&1 &
    
./bin/kafka-server-start etc/kafka/server.properties > /dev/null 2>&1 &
    
./bin/schema-registry-start etc/schema-registry/schema-registry.properties > /dev/null 2>&1 &
```






-. Download MySQL connector for Java.

```
wget  http://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-5.1.30.tar.gz
tar -zxvf mysql-connector-java-5.1.30.tar.gz && cd mysql-connector-java-5.1.30
cp mysql-connector-java-5.1.30-bin.jar /home/sergio/confluent-5.0.0/confluent-5.0.0/share/java/kafka-connect-jdbc/
```



-.Configure DATA SOURCE 

```
nano /home/sergio/confluent-5.0.0/source-quickstart-mysql.properties
```

```
name=test-source-mysql-jdbc-autoincrement
connector.class=io.confluent.connect.jdbc.JdbcSourceConnector
tasks.max=1
connection.url=jdbc:mysql://127.0.0.1:3306/employees?user=root&password=root
table.whitelist=fingerprint_events
mode=timestamp
timestamp.column.name=lastUpdated
topic.prefix=topic-
```


-.Configure DATA SINK 

```
nano /home/sergio/confluent-5.0.0/etc/kafka-connect-jdbc/sink-quickstart-mysql.properties
```


```
name=test-sink-mysql-jdbc-autoincrement
connector.class=io.confluent.connect.jdbc.JdbcSinkConnector
tasks.max=1
topics=topic-fingerprint_events
connection.url=jdbc:mysql://localhost:3306/employees?user=root&password=root
auto.create=true
```



download kafka connect cli jar :

``` 
wget https://github.com/Landoop/kafka-connect-tools/releases/download/v1.0.6/kafka-connect-cli-1.0.6-all.jar 
```


Start standalone connector

```./bin/connect-standalone etc/schema-registry/connect-avro-standalone.properties source-quickstart-mysql.properties```

Register Connector : 

```
java -jar kafka-connect-cli-0.5-all.jar create mysql-sink-employees < sink-quickstart-mysql.properties 
```

Test topic : 

```
./bin/kafka-avro-console-consumer  --bootstrap-server localhost:9092   -from-beginning  --topic topic-fingerprint_events
```


# Reception of asynchronous notifications :


Install Spark :

```
wget https://www.apache.org/dyn/closer.lua/spark/spark-2.3.2/spark-2.3.2-bin-hadoop2.7.tgz
```

Run spark-shell :

```
./bin/spark-shell --packages org.apache.spark:spark-sql-kafka-0-10_2.11:2.3.0 --jars mysql-connector-java-5.0.8-bin.jar

```

Paste the next code (first activate special paste with : paste ) :

```
import org.apache.spark.sql.functions.{get_json_object, json_tuple}
import java.sql._
import org.apache.spark.sql.ForeachWriter
import org.apache.spark.sql.streaming.ProcessingTime
import org.apache.spark.sql.Row



 
var streamingSelectDF = 
  spark.readStream
    .format("kafka")
    .option("kafka.bootstrap.servers", "localhost:9092")
    .option("subscribe", "topic-fingerprint_events")     
    .option("startingOffsets", "latest")  
    .option("minPartitions", "10")  
    .option("failOnDataLoss", "true")
    .load()




class  JDBCSink(url:String, user:String, pwd:String) extends ForeachWriter[Row] {
      val driver = "com.mysql.jdbc.Driver"
      var connection:Connection = _
      var statement:Statement = _
      
    def open(partitionId: Long,version: Long): Boolean = {
        Class.forName(driver)
        connection = DriverManager.getConnection(url, user, pwd)
        statement = connection.createStatement
        true
      }

      // check if status equal to 0 (out of work) and the hour is less than 18:00
      def process(value: (String, String)): Unit = {

        if(value_3 == 0) {
           statement.executeUpdate("INSERT INTO absence_anomalies " + 
                "VALUES (" + value._1 + "," + value._2 + ")")
        }
      }

      def close(errorOrNull: Throwable): Unit = {
        connection.close
      }
   }


val url="jdbc:mysql://127.0.0.1:3306/employees"
val user ="root"
val pwd = "root"

val writer : JDBCSink = new JDBCSink(url,user, pwd)
val query =
  streamingSelectDF
    .writeStream
    .foreach(writer)
    .outputMode("update")
    .trigger(ProcessingTime("25 seconds"))
    .start()
    
```




# Posible Improvements :

1.Deploy Kafka connect in distributed mode .

2.Mysql with HA .

3.Spark in cluster mode .








