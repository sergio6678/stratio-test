drop database IF EXISTS employees;


create database employees ;


use employees ;

CREATE TABLE employees_detail (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
firstname VARCHAR(30) NOT NULL,
lastname VARCHAR(30) NOT NULL,
email VARCHAR(50),
reg_date TIMESTAMP
);


insert into employees_detail(firstname,lastname,email,reg_date) values ('Homero','Simpson','h.simpson@gmail.com',now());
insert into employees_detail(firstname,lastname,email,reg_date) values ('Carl','Carlson','c.carlson@gmail.com',now());
insert into employees_detail(firstname,lastname,email,reg_date) values ('Lenny','Leonard','l.leonard@gmail.com',now());
insert into employees_detail(firstname,lastname,email,reg_date) values ('Frank','Grimes','f.grimes@gmail.com',now());
insert into employees_detail(firstname,lastname,email,reg_date) values ('NN','Zutroy','n.zutroy@gmail.com',now());



CREATE TABLE employees_presence(
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
id_employee INT(6),
status BOOLEAN,
ts TIMESTAMP, 
dt DATETIME
);


insert into employees_presence(id_employee,status,ts,dt) values (1,1,now(),now());
insert into employees_presence(id_employee,status,ts,dt) values (2,1,now(),now());
insert into employees_presence(id_employee,status,ts,dt) values (3,1,now(),now());
insert into employees_presence(id_employee,status,ts,dt) values (4,1,now(),now());
insert into employees_presence(id_employee,status,ts,dt) values (5,1,now(),now());


CREATE TABLE fingerprint_events(
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
id_employee INT(6),
status BOOLEAN,
ts TIMESTAMP, 
dt DATETIME
);


insert into fingerprint_events(id_employee,status,ts,dt) values (1,1,now(),now());
insert into fingerprint_events(id_employee,status,ts,dt) values (2,1,now(),now());
insert into fingerprint_events(id_employee,status,ts,dt) values (3,1,now(),now());
insert into fingerprint_events(id_employee,status,ts,dt) values (4,1,now(),now());
insert into fingerprint_events(id_employee,status,ts,dt) values (5,1,now(),now());


CREATE TABLE absence_anomalies(
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
id_employee INT(6),
ts TIMESTAMP, 
dt DATETIME
);

insert into absence_anomalies(id_employee,ts,dt) values (1,now(),now());
insert into absence_anomalies(id_employee,ts,dt) values (2,now(),now());
insert into absence_anomalies(id_employee,ts,dt) values (3,now(),now());

