from flask import Flask, render_template, request
import pymysql


 
app = Flask(__name__)
 
 
class Database:
    def __init__(self):
        host = "127.0.0.1"
        user = "root"
        password = "root"
        db = "employees"
 
        self.con = pymysql.connect(host=host, user=user, password=password, db=db, cursorclass=pymysql.cursors.
                                   DictCursor)
        self.cur = self.con.cursor()
 
    def list_employees(self):
        self.cur.execute("SELECT firstname, lastname, email FROM employees_detail LIMIT 50")
        result = self.cur.fetchall()
 
        return result

    def list_employees_on(self):
        self.cur.execute("select * from employees_presence join employees_detail on employees_detail.id =employees_presence.id_employee  where status = true;")
        result = self.cur.fetchall()
 
        return result

    def list_employees_absence(self):
        self.cur.execute("select * from employees_detail join absence_anomalies  on employees_detail.id = absence_anomalies.id_employee;")
        result = self.cur.fetchall()
 
        return result
 
@app.route('/employees')
def employees():
 
    def db_query():
        db = Database()
        emps = db.list_employees()
 
        return emps
 
    res = db_query()
 
    return render_template('employees.html', result=res, content_type='application/json')


@app.route('/employees_on')
def employees_on():
 
    def db_query():
        db = Database()
        emps = db.list_employees_on()
 
        return emps
 
    res = db_query()
 
    return render_template('employees_on.html', result=res, content_type='application/json')



@app.route('/employees_absence')
def employees_abscence():
 
    def db_query():
        db = Database()
        emps = db.list_employees_absence()
 
        return emps
 
    res = db_query()
 
    return render_template('employees_anomalies.html', result=res, content_type='application/json')



@app.route('/employee/<labelname>')
def show_results(labelname=None):
    
    print(request.args.get(id))

    return render_template('employees_detail.html',  content_type='application/json')





@app.route('/')
def hello_world():
    return render_template('index.html',content_type='application/json')